# kademlia_routing_table - Change Log

## [0.0.4]
- Add bucket index to NodeInfo<T,U>
- hash_node -> get (return option & NodeInfo)
- reduced some if statements with if/else if blocks
## [0.0.3]
- Remove unneeded library (clippy)
- Fixed typo in parallelism methods
## [0.0.2]
- Added functions to return constant values as usize
- Moved constants to u8
- Added function to return dynamic quorum size
- unpublished in crates.io
## [0.0.1]
- Initial Implementation
